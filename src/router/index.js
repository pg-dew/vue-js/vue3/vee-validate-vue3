import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import FieldLevel from "@/views/validation/FieldLevel";
import FieldLevelYup from "@/views/validation/FieldLevelYup";
import FormLevel from "@/views/validation/FormLevel";
import FormLevelYup from "@/views/validation/FormLevelYup";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/validation/field/1",
    component: FieldLevel,
  },
  {
    path: "/validation/field/2",
    component: FieldLevelYup,
  },
  {
    path: "/validation/form/1",
    component: FormLevel,
  },
  {
    path: "/validation/form/2",
    component: FormLevelYup,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
